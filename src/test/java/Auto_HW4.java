import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

class PokerPlayer {
    String Username;
    String Email;
    String Password;
    String Confirm_Password;
    String FirstName;
    String LastName;
    String City;
    String County;
    String Address;
    String Phone;
}

public class Auto_HW4 {
    ///////     Variable definition     ///////
    //Title page
    static final String TITLE_LOGIN_PAGE = "Login";
    static final String TITLE_PLAYERS_PAGE = "Players";
    static final String TITLE_INSERT_PAGE = "Players - Insert";
    static final String TITLE_EDIT_PAGE = "Players - Edit";

    //Data
    static final String OPENED_LINK_PAGE = "http://80.92.229.235/auth/login";
    static final String LOG_USERNAME = "admin";
    static final String LOG_PASSWORD = "test";

    //p1
    static final String DATA_P1_USERNAME = "Zx" + ((int) Math.floor(Math.random()*(999999 - 9999))+9999);
    static final String DATA_P1_EMAIL = DATA_P1_USERNAME + "@ukr.net";
    static final String DATA_P1_PASSWORD = "Qwerty@123456";
    static final String DATA_P1_CONFIRM_PASSWORD = "Qwerty@123456";
    static final String DATA_P1_FIRSTNAME = "Auto_FirstName";
    static final String DATA_P1_LASTNAME = "Auto_LastName";
    static final String DATA_P1_CITY = "Kharkiv";
    static final String DATA_P1_COUNTRY = "UKRAINE";
    static final String DATA_P1_ADDRESS = "Nauky ave. 14";
    static final String DATA_P1_PHONE = "0001234567";

    //p3
    static final String DATA_P3_USERNAME = "Zx" + ((int) Math.floor(Math.random()*(999999 - 9999))+9999);
    static final String DATA_P3_EMAIL = DATA_P3_USERNAME + "@ukr.net";
    static final String DATA_P3_PASSWORD = "Qwerty@123456";
    static final String DATA_P3_CONFIRM_PASSWORD = "Qwerty@123456";
    static final String DATA_P3_FIRSTNAME = "Auto_FirstName_P3";
    static final String DATA_P3_LASTNAME = "Auto_LastName_P3";
    static final String DATA_P3_CITY = "Kharkiv";
    static final String DATA_P3_COUNTRY = "UKRAINE";
    static final String DATA_P3_ADDRESS = "Nauky ave. XX";
    static final String DATA_P3_PHONE = "9991234567";

    //p3 upgrade player
    static final String DATA_P2_EMAIL = DATA_P3_USERNAME + "@gmail.net";
    static final String DATA_P2_FIRSTNAME = "Upgrade_FirstName";
    static final String DATA_P2_LASTNAME = "Upgrade_LastName";
    static final String DATA_P2_CITY = "Kyiv";
    static final String DATA_P2_COUNTRY = "CANADA";
    static final String DATA_P2_ADDRESS = "Lenina ave. 14";
    static final String DATA_P2_PHONE = "1117654321";

    //p4
    static final String DATA_P4_USERNAME = "Zxc" + ((int) Math.floor(Math.random()*(999999 - 999))+999);
    static final String DATA_P4_EMAIL = DATA_P4_USERNAME + "@ukr.net";
    static final String DATA_P4_PASSWORD = "Qwerty@123456";
    static final String DATA_P4_CONFIRM_PASSWORD = "Qwerty@123456";
    static final String DATA_P4_FIRSTNAME = "Auto_FirstName_P4";
    static final String DATA_P4_LASTNAME = "Auto_LastName_P4";
    static final String DATA_P4_CITY = "Kharkiv";
    static final String DATA_P4_COUNTRY = "UKRAINE";
    static final String DATA_P4_ADDRESS = "Nauky ave. XXX";
    static final String DATA_P4_PHONE = "2221234560";


    //Field identifiers
    static final String IDEN_LOG_USERNAME_FIELD = "username";
    static final String IDEN_LOG_PASSWORD_FIELD = "password";
    static final String IDEN_LOG_BUTTON = "logIn";
    static final String IDEN_INSERT_BUTTON = "//a[contains(text(), \"Insert\")]";
    static final String IDEN_INS_USERNAME_FIELD = "us_login";
    static final String IDEN_INS_EMAIL_FIELD = "us_email";
    static final String IDEN_INS_PASSWORD_FIELD = "us_password";
    static final String IDEN_INS_CONFIRM_PASSWORD_FIELD = "confirm_password";
    static final String IDEN_INS_FIRSTNAME_FIELD = "us_fname";
    static final String IDEN_INS_LASTNAME_FIELD = "us_lname";
    static final String IDEN_INS_COUNTRY_FIELD = "us_country";
    static final String IDEN_INS_CITY_FIELD = "us_city";
    static final String IDEN_INS_ADDRESS_FIELD = "us_address";
    static final String IDEN_INS_PHONE_FIELD = "us_phone";
    static final String IDEN_INS_SUBMIT_BUTTON = "Submit";
    static final String IDEN_SEARCH_PLAEYR_FIELD = "login";
    static final String IDEN_SEARCH_BUTTON = "//button[contains(text(), \"Search\")]";
    static final String IDEN_RESET_BUTTON = "//button[contains(text(), \"Reset\")]";
    static final String IDEN_EDIT_USER = "//a[text() = \"" + DATA_P3_USERNAME + "\"]/../../td[1]/a";
    static final String IDEN_DELETE_USER = "//a[text() = \"" + DATA_P1_USERNAME + "\"]/../../td[16]/a";
    static final String IDEN_DELETE_USER_P4 = "//a[text() = \"" + DATA_P4_USERNAME + "\"]/../../td[16]/a";
    static final String IDEN_CONFIRM_DELETE_USER ="//td[@class = \"dataTables_empty\"]";
    static final String IDEN_VERIFY_USER_START = "//a[text() = \"";
    static final String IDEN_VERIFY_USER_END = "\"]/../../td[1]/a";
    static final String IDEN_LOGOUT_BUTTON = "//div[@class=\"logout\"]/a";


    public static WebDriver driver;
    public static PokerPlayer p1 = new PokerPlayer();
    public static PokerPlayer p2 = new PokerPlayer();
    public static PokerPlayer p3 = new PokerPlayer();
    public static PokerPlayer p4 = new PokerPlayer();

    public Auto_HW4(){
        System.out.println("Start TESTING");
    }


    @BeforeSuite(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void SetupWebDriver (){

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        System.out.println("SetupWebDriver: OK");

        //SetupTestData
        p1.Username = DATA_P1_USERNAME;
        p1.Email = DATA_P1_EMAIL;
        p1.Password = DATA_P1_PASSWORD;
        p1.Confirm_Password = DATA_P1_CONFIRM_PASSWORD;
        p1.FirstName = DATA_P1_FIRSTNAME;
        p1.LastName = DATA_P1_LASTNAME;
        p1.City = DATA_P1_CITY;
        p1.County = DATA_P1_COUNTRY;
        p1.Address = DATA_P1_ADDRESS;
        p1.Phone = DATA_P1_PHONE;

        p3.Username = DATA_P3_USERNAME;
        p3.Email = DATA_P3_EMAIL;
        p3.Password = DATA_P3_PASSWORD;
        p3.Confirm_Password = DATA_P3_CONFIRM_PASSWORD;
        p3.FirstName = DATA_P3_FIRSTNAME;
        p3.LastName = DATA_P3_LASTNAME;
        p3.City = DATA_P3_CITY;
        p3.County = DATA_P3_COUNTRY;
        p3.Address = DATA_P3_ADDRESS;
        p3.Phone = DATA_P3_PHONE;

        p2.Username = DATA_P3_USERNAME;
        p2.Email = DATA_P2_EMAIL;
        p2.FirstName = DATA_P2_FIRSTNAME;
        p2.LastName = DATA_P2_LASTNAME;
        p2.City = DATA_P2_CITY;
        p2.County = DATA_P2_COUNTRY;
        p2.Address = DATA_P2_ADDRESS;
        p2.Phone = DATA_P2_PHONE;

        p4.Username = DATA_P4_USERNAME;
        p4.Email = DATA_P4_EMAIL;
        p4.Password = DATA_P4_PASSWORD;
        p4.Confirm_Password = DATA_P4_CONFIRM_PASSWORD;
        p4.FirstName = DATA_P4_FIRSTNAME;
        p4.LastName = DATA_P4_LASTNAME;
        p4.City = DATA_P4_CITY;
        p4.County = DATA_P4_COUNTRY;
        p4.Address = DATA_P4_ADDRESS;
        p4.Phone = DATA_P4_PHONE;
        System.out.println("SetupTestData: OK");

    }

    @AfterSuite(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void closeWebDriver(){
        driver.quit();
        System.out.println("Close the Chrome");
    }

    @BeforeTest(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void preconditions(){
        //Login();
        driver.manage().window().maximize();
        driver.get(OPENED_LINK_PAGE);

        //wait open Login page -> wait Clickable LogIn button

        WebDriverWait wait = new WebDriverWait(driver, 10);
        WebElement LogIn_button = wait.until(ExpectedConditions.elementToBeClickable(By.id(IDEN_LOG_BUTTON)));

        //check the Title of page - "Login"
        Assert.assertEquals(driver.getTitle(), TITLE_LOGIN_PAGE);

        WebElement username = driver.findElement(By.id(IDEN_LOG_USERNAME_FIELD));
        WebElement password = driver.findElement(By.id(IDEN_LOG_PASSWORD_FIELD));
        WebElement LogInbutton = driver.findElement(By.id(IDEN_LOG_BUTTON));

        username.sendKeys(LOG_USERNAME);
        password.sendKeys(LOG_PASSWORD);
        LogInbutton.click();

        System.out.println("Operation Login: OK");

    }

    @AfterTest(groups = {"CreatePlayer", "EditPlayer", "DeletePlayer"})
    public void LogOut() {

        //wait visibility Player field and enter Username
        WebDriverWait wait = new WebDriverWait(driver, 5);

        //wait Clickable Search button and click
        WebElement search_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_LOGOUT_BUTTON)));
        search_button.click();

        Assert.assertEquals(driver.getTitle(), TITLE_LOGIN_PAGE);
        System.out.println("LogOut: OK");
    }

    @Test(groups = {"CreatePlayer"}, priority = 0)
    public void Create_Player(){
        CreatePlayer(p1);
        Find_and_VerifyPlayer(p1);
        DeletePlayer(p1);
    }

    @Test(groups = {"EditPlayer"}, priority = 1)
    public void EditPlayer(){
        CreatePlayer(p3);
        Find_and_EditPlayer(p3, p2);
        Find_and_VerifyPlayer(p2);
    }

    @Test(groups = {"DeletePlayer"}, priority = 2)
    public void DeletePlayer(){
        CreatePlayer(p4);
        Find_and_Verify_and_DeletePlayer(p4);
    }

    static void CreatePlayer (PokerPlayer p0) {

        //wait Clickable Insert button
        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement Insert_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_INSERT_BUTTON)));
        Insert_button.click();

        //check the Title of page - "Players - Insert"
        Assert.assertEquals(driver.getTitle(), TITLE_INSERT_PAGE);

        WebElement username_field = driver.findElement(By.id(IDEN_INS_USERNAME_FIELD));
        WebElement email_field = driver.findElement(By.id(IDEN_INS_EMAIL_FIELD));
        WebElement password_field = driver.findElement(By.id(IDEN_INS_PASSWORD_FIELD));
        WebElement confirm_field = driver.findElement(By.id(IDEN_INS_CONFIRM_PASSWORD_FIELD));
        WebElement firstname = driver.findElement(By.id(IDEN_INS_FIRSTNAME_FIELD));
        WebElement lastname = driver.findElement(By.id(IDEN_INS_LASTNAME_FIELD));
        WebElement county = driver.findElement(By.id(IDEN_INS_COUNTRY_FIELD));
        WebElement city = driver.findElement(By.id(IDEN_INS_CITY_FIELD));
        WebElement address = driver.findElement(By.id(IDEN_INS_ADDRESS_FIELD));
        WebElement phone = driver.findElement(By.id(IDEN_INS_PHONE_FIELD));
        WebElement submit_button = driver.findElement(By.id(IDEN_INS_SUBMIT_BUTTON));

        username_field.sendKeys(p0.Username);
        email_field.sendKeys(p0.Email);
        password_field.sendKeys(p0.Password);
        confirm_field.sendKeys(p0.Confirm_Password);
        firstname.sendKeys(p0.FirstName);
        lastname.sendKeys(p0.LastName);
        county.sendKeys(p0.County);
        city.sendKeys(p0.City);
        address.sendKeys(p0.Address);
        phone.sendKeys(p0.Phone);
        submit_button.click();

        System.out.println("Operation Create Player: OK");
    }

    static void Find_and_VerifyPlayer (PokerPlayer p0) {
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait visibility Player field and enter Username
        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement player_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IDEN_SEARCH_PLAEYR_FIELD)));
        player_field.sendKeys(p0.Username);
        System.out.println("Search Player: " + p0.Username);

        //wait Clickable Search button and click
        WebElement search_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_SEARCH_BUTTON)));
        search_button.click();

        //wait Clickable Edit button and click

        String IDEN_VERIFY_USER = IDEN_VERIFY_USER_START + p0.Username + IDEN_VERIFY_USER_END;
        WebElement edit_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_VERIFY_USER)));
        edit_button.click();

        //wait email field
        WebElement email_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IDEN_INS_EMAIL_FIELD)));
        Assert.assertEquals(driver.getTitle(), TITLE_EDIT_PAGE);

        PokerPlayer p3 = new PokerPlayer();
        p3.Email = driver.findElement(By.id(IDEN_INS_EMAIL_FIELD)).getAttribute("value");
        p3.FirstName = driver.findElement(By.id(IDEN_INS_FIRSTNAME_FIELD)).getAttribute("value");
        p3.LastName = driver.findElement(By.id(IDEN_INS_LASTNAME_FIELD)).getAttribute("value");
        p3.City = driver.findElement(By.id(IDEN_INS_CITY_FIELD)).getAttribute("value");
        p3.Address = driver.findElement(By.id(IDEN_INS_ADDRESS_FIELD)).getAttribute("value");
        p3.Phone = driver.findElement(By.id(IDEN_INS_PHONE_FIELD)).getAttribute("value");

        Select select = new Select(driver.findElement(By.id(IDEN_INS_COUNTRY_FIELD)));
        p3.County = select.getFirstSelectedOption().getText();

        //for debugging
        //System.out.println("\nRead data Player:\n" + p3.Email + "\n" + p3.FirstName + "\n" + p3.LastName + "\n"  + p3.City + "\n"  + p3.County + "\n"  + p3.Address + "\n"  + p3.Phone);
        //System.out.println("\nSet data Player:\n" + p0.Email + "\n" + p0.FirstName + "\n" + p0.LastName + "\n"  + p0.City + "\n"  + p0.County + "\n"  + p0.Address + "\n"  + p0.Phone);

        SoftAssert softAssert=new SoftAssert();
        softAssert.assertEquals(p0.Email, p3.Email, "The value in the email field does not match");
        softAssert.assertEquals(p0.FirstName, p3.FirstName, "The value in the FirstName field does not match");
        softAssert.assertEquals(p0.LastName, p3.LastName, "The value in the LastName field does not match");
        softAssert.assertEquals(p0.City, p3.City, "The value in the City field does not match");
        softAssert.assertEquals(p0.County, p3.County, "The value in the County field does not match");
        softAssert.assertEquals(p0.Address, p3.Address, "The value in the Address field does not match");
        softAssert.assertEquals(p0.Phone, p3.Phone, "The value in the Phone field does not match");

        softAssert.assertAll();

        System.out.println("\nVerify data Player - OK");

        WebElement submit_button = driver.findElement(By.id(IDEN_INS_SUBMIT_BUTTON));
        submit_button.click();
    }

    static void Find_and_EditPlayer (PokerPlayer p0, PokerPlayer p0_new) {
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait visibility Player field and enter Username
        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement player_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IDEN_SEARCH_PLAEYR_FIELD)));
        player_field.sendKeys(p0.Username);
        System.out.println("Search Player: " + p0.Username);

        //wait Clickable Search button and click
        WebElement search_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_SEARCH_BUTTON)));
        search_button.click();

        //wait Clickable Edit button and click
        WebElement edit_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_EDIT_USER)));
        edit_button.click();

        //wait email field
        WebElement email_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IDEN_INS_EMAIL_FIELD)));
        Assert.assertEquals(driver.getTitle(), TITLE_EDIT_PAGE);


        //replace data player

        WebElement email_field1 = driver.findElement(By.id(IDEN_INS_EMAIL_FIELD));
        WebElement firstname1 = driver.findElement(By.id(IDEN_INS_FIRSTNAME_FIELD));
        WebElement lastname1 = driver.findElement(By.id(IDEN_INS_LASTNAME_FIELD));
        WebElement county1 = driver.findElement(By.id(IDEN_INS_COUNTRY_FIELD));
        WebElement city1 = driver.findElement(By.id(IDEN_INS_CITY_FIELD));
        WebElement address1 = driver.findElement(By.id(IDEN_INS_ADDRESS_FIELD));
        WebElement phone1 = driver.findElement(By.id(IDEN_INS_PHONE_FIELD));
        WebElement submit_button1 = driver.findElement(By.id(IDEN_INS_SUBMIT_BUTTON));

        email_field1.clear();
        firstname1.clear();
        lastname1.clear();
        city1.clear();
        address1.clear();
        phone1.clear();

        email_field1.sendKeys(p0_new.Email);
        firstname1.sendKeys(p0_new.FirstName);
        lastname1.sendKeys(p0_new.LastName);
        county1.sendKeys(p0_new.County);
        city1.sendKeys(p0_new.City);
        address1.sendKeys(p0_new.Address);
        phone1.sendKeys(p0_new.Phone);
        submit_button1.click();

        System.out.println("Replace data Player: OK");
    }

    static void Find_and_Verify_and_DeletePlayer (PokerPlayer p0) {
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait visibility Player field and enter Username
        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement player_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IDEN_SEARCH_PLAEYR_FIELD)));
        player_field.sendKeys(p0.Username);
        System.out.println("Search Player: " + p0.Username);

        //wait Clickable Search button and click
        WebElement search_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_SEARCH_BUTTON)));
        search_button.click();

        //wait Clickable Delete button and click
        WebElement delete_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_DELETE_USER_P4)));
        delete_button.click();

        //Alert, click ОК
        Alert alert;
        alert = driver.switchTo().alert();
        alert.accept();
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait Clickable Reset button and click
        WebElement reset_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_RESET_BUTTON)));
        reset_button.click();
        System.out.println("Delete Player: " + p0.Username + " - OK");


        //Confirm delete Player
        player_field.sendKeys(p0.Username);
        //wait Clickable Search button and click
        search_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_SEARCH_BUTTON)));
        search_button.click();

        WebElement confirm_del_user = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_CONFIRM_DELETE_USER)));
        System.out.println("Delete Player: " + p0.Username + " - CONFIRM");
    }

    static void DeletePlayer (PokerPlayer p0) {
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait visibility Player field and enter Username
        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement player_field = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(IDEN_SEARCH_PLAEYR_FIELD)));
        player_field.sendKeys(p0.Username);
        System.out.println("Search Player: " + p0.Username);

        //wait Clickable Search button and click
        WebElement search_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_SEARCH_BUTTON)));
        search_button.click();

        //wait Clickable Delete button and click
        WebElement delete_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_DELETE_USER)));
        delete_button.click();

        //Alert, click OK
        Alert alert;
        alert = driver.switchTo().alert();
        alert.accept();
        Assert.assertEquals(driver.getTitle(), TITLE_PLAYERS_PAGE);

        //wait Clickable Reset button and click
        WebElement reset_button = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(IDEN_RESET_BUTTON)));
        reset_button.click();

        System.out.println("Delete Player: " + p0.Username + " - OK");
    }

}